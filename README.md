#Its a HoneyPot, K?


##What is it?

"HoneyPot as a service". Demonstrates the possible security risks in using shady URL shorteners. 

The web app allows the user to track multiple campaigns, with an email alert whenever a victim clicks honeypot url. 
A database saves the user agent and external IP of the victim. 

I also like it because its one of the few instances of a .tk domain hack. So that's nice. 


##Getting Started

* Clone repository
* Enter SendGrid and MongoDB credentials
* Do ``npm install``
* Ready to roll

##Where can I find it?
Production version can be found at [itsahoneypo.tk](http://itsahoneypo.tk)


##License
MIT