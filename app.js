/*****************************
 * Author: Akhil Acharya
 * Project: ItsAHoneyPo.tk
 * Started: 2/8/14
 ******************************/

var express = require('express');

var config = require("./config.json");

//DB
var mongoose = require('mongoose');

var db_url = config.db.base_url + config.db.username + ":" + config.db.password + config.db.path;

mongoose.connect(db_url);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection error'));

db.once('open', function callback() {
    console.log("Connected to MongoDB");
})


//App
var app = module.exports = express.createServer();

//Instantiate Models
require('./models/HoneyPotModel.js');
require('./models/VictimModel.js'); 


//Controllers
var HoneyController = require('./controllers/HoneyController.js')(mongoose, config);

//Configuration
app.configure(function() {
    app.use(express.bodyParser());
    app.use(express.cookieParser());
    app.use(express.session({
        secret: "I'm actually Sri Lankan"
    }));
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(app.router);
    app.use(express.static(__dirname + '/public'));
});

app.configure('development', function() {
    app.use(express.errorHandler({
        dumpExceptions: true,
        showStack: true
    }));
});

app.configure('production', function() {
    app.use(express.errorHandler());
});


// Routes
app.get('/', HoneyController.createPot);
app.get('/:id', HoneyController.getPot);

//Data POSTed by Angular during creation
app.post('/savePot', HoneyController.savePot); 


//Listen in
app.listen(8081, function() {
    console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
    console.log("Currently in testing mode");
});
