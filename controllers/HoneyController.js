module.exports = function(mongoose, config) {

    var HoneyPot = mongoose.model("HoneyPot");
    var Victim = mongoose.model('Victim');

    var moment = require('moment');
    var sendgrid = require('sendgrid')(config.sendgrid.api_user, config.sendgrid.api_key);

  
    var routes = {

        //GET Routes    
        createPot: function(req, res) {

            var ip = req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress;

            res.render('CreatePot', {
                ip: ip
            });
        },

        getPot: function(req, res) {
            //DB Query here
            var id = req.params.id;

            HoneyPot.findOne({
                id: id
            }, function(err, honeypot) {
                if (err || !honeypot) {
                    res.send("404");
                } else {

                    var ip = req.headers['x-forwarded-for'] ||
                        req.connection.remoteAddress ||
                        req.socket.remoteAddress ||
                        req.connection.socket.remoteAddress;
                

                    if (honeypot.whitelisted.indexOf(ip) == -1) {

                        var userAgent = req.headers['user-agent'];
                        var victim = new Victim({
                            campaign: honeypot.campaign,
                            userAgent: userAgent,
                            ip: ip,
                            dateAccessed: new Date(),
                        })

                        sendgrid.send({
                            to: honeypot.alertEmail,
                            from: "HoneypotAlerts@itsahoneypo.tk",
                            subject: "Update on campaign " + '"' + victim.campaign + '"',
                            text: getAlertEmail(victim),
                        }, function(err, json) {
                            if (err) {
                                return console.error(err)
                            }
                        });
                        honeypot.victims.push(victim);
                    } else {
                        console.log("Whitelisted IP detected");
                    }

                    honeypot.save(function(err) {
                        res.render('RedirectPage', {
                            redirect: honeypot.redirect,
                        });
                    });
                }
            });
        },

        //POST Routes
        savePot: function(req, res) {
            //Accept POST and do DB save here 

            var postedPot = req.body;

            var newHoneyPot = new HoneyPot({
                id: getId(),
                campaign: postedPot.campaign,
                redirect: (function(){
                    var queryRegex = new RegExp("^https?://");
                    if(postedPot.redirectURL.match(queryRegex)){
                        return postedPot.redirectURL; 
                    }else{
                        return "http://" + postedPot.redirectURL; 
                    }
                })(),
                whitelisted: postedPot.whitelistedIPs,
                alertEmail: postedPot.email,
            });

            newHoneyPot.save(function(err) {
                if (err) {
                    res.send({
                        status: 400
                    });
                } else {
                    res.send({
                        status: 200,
                        id: newHoneyPot.id
                    });
                }
            })
        },
    };

    function getId() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    function getAlertEmail(victim) {

        var parser = require('ua-parser').parse(victim.userAgent); 

        var device = parser.device.family; 
        var OS = parser.os.family.toString(); 

        var agent = parser.ua.toString(); 

    

        var message = "Greetings, \n\n" 

                        + "Your Honeypot was accessed at " 
                        + moment().format('MMMM Do YYYY, h:mm:ss a')
                        + ".\n\n" + "The IP was " + victim.ip 
                        + " and the victim was using a device of the following useragent: \n\n" 
                        + "The victim appears to be using a(n) " + device + " device running " + OS + " using the " + agent + " browser.\n\n"
                        + "The raw user agent follows: \n\n"
                        + victim.userAgent + ". \n\n" 
                        + "Thanks for using our service.\n\n\n" + "Sincerely, \n" + "The team at itsahoneypo.tk";

        return message;
    }


    return routes;
}
