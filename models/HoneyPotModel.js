var mongoose = require('mongoose'); 

var honeyPotSchema = mongoose.Schema({
	id: String, 
	campaign: String, 
	redirect: String, 
	//Array of Victim Objects
	victims: Array, 
	//Array of Whitelisted IPs
	whitelisted: Array, 
	alertEmail: String, 
}); 


mongoose.model("HoneyPot", honeyPotSchema); 

