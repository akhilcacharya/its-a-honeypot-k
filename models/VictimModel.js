var mongoose = require('mongoose'); 

var victimSchema = mongoose.Schema({
	id: String, 
	ip: String, 
	campaign: String, 
	userAgent: String,  
	dateAccessed: Date, 
}); 


mongoose.model("Victim", victimSchema); 

