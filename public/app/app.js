function AppCtrl($scope, $http){

	$scope.alert = ""; 

	$scope.redirectURL = ""; 
	$scope.email = ""; 
	$scope.campaign = ""; 

	$scope.whitelistedIPs = [];
	$scope.whitelistedIPs.push({ip: homeip, index : 0}); 

	$scope.ip = ""; 

	var index = 1; 

	$scope.addIP = function(){
		$scope.whitelistedIPs.push({index: index++, ip: $scope.ip});
		$scope.ip = "";  
	}

	$scope.removeIP = function(ip){
		for(var i = 0; i < $scope.whitelistedIPs.length; i++){
			if($scope.whitelistedIPs[i].index == ip.index){
				$scope.whitelistedIPs.splice(i, 1); 
				break; 
			}else{
				continue; 
			}
		}
	}

	$scope.post = function(){

		var ips = []; 
		for(var i = 0; i < $scope.whitelistedIPs.length; i++){
			ips.push($scope.whitelistedIPs[i].ip); 
		}

		var honeypot = {
			campaign: $scope.campaign, 
			redirectURL: $scope.redirectURL, 
			email: $scope.email, 
			whitelistedIPs: ips, 
		}

		$http.post('/savePot', honeypot).success(function(data){
			if(data.status == 200){
				$scope.alert = "Send Victim this URL: www.itsahoneypo.tk/" + data.id; 
			}
		}).error(function(err){
			console.log(err); 
		})

		console.log(honeypot); 

	}; 


}

